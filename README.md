### Hola 👋<h2> Soy Benjamin</h2>

<img align='right' src="https://raw.githubusercontent.com/iCharlesZ/FigureBed/master/img/octocat.gif" width="230">

<h3> 👨🏻•💻 Acerca de mí </h3>


- 🎓 &nbsp; Estudiante de Ingeniería de Sistemas e Informatica .

- 🌱 &nbsp; Aprendiendo sobre Cloud Computing y desarrollo web.

- 🤔 &nbsp; Explorando nuevas tecnologías y desarrollando soluciones de software.

- ✍️ &nbsp; Seguir el desarrollo web como pasatiempos / actividades secundarias.



<h3>🛠Pila de tecnología</h3>



- 💻 &nbsp; PHP | Java | C++ 

- 🌐 &nbsp; HTML | CSS | JavaScript  

- 🛢 &nbsp; MySQL 

- 🔧 &nbsp;   Laravel | Sass |Bootstrap 5
<!--

- 🛢 &nbsp; MySQL | MongoDB

- 🔧 &nbsp; Git | Markdown | Selenium | Tidyverse

- 🖥 &nbsp; Illustrator| Photoshop | InDesign

-->



<h3>🛠 Por aprender</h3>

- 🔧 &nbsp; AWS | Docker🐳 |kubernetes | jenkins 

<hr>



<br/><br/>

[![Benjamin SH GitHub Stats](https://github-readme-stats.vercel.app/api?username=benjaminsucasaire&show_icons=true)](https://github.com/benjaminsucasaire)

<br/>

<br/>

<img src="https://media.giphy.com/media/M9gbBd9nbDrOTu1Mqx/giphy.gif" width="350" align='right'>

![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=benjaminsucasaire&show_icons=true)

<br><br>



<hr>



<h3> 🤝🏻Contactate conmigo </h3>

<br>



<p align="center">

<a href="https://benjamin.sistemascloudcyb.ml/"><img alt="Website" src="https://img.shields.io/badge/benjamin.sistemascloudcyb.ml-black?style=flat-square&logo=google-chrome"></a>

<a href="https://www.linkedin.com/in/benjamin-abel-sucasaire-huamani-719624182/"><img alt="LinkedIn" src="https://img.shields.io/badge/LinkedIn-Benjamin%20Abel%20Sucasaire-blue?style=flat-square&logo=linkedin"></a>

<a href="https://www.instagram.com/benjamin_abue/"><img alt="Instagram" src="https://img.shields.io/badge/Instagram-benjamin_abue-black?style=flat-square&logo=instagram"></a>

<a href="mailto:benjaminsucasaire@gmail.com"><img alt="Email" src="https://img.shields.io/badge/Email-benjaminsucasaire@gmail.com-blue?style=flat-square&logo=gmail"></a>

</p>





![Visitor count](https://visitor-badge.laobi.icu/badge?page_id=benjaminsucasaire.benjaminsucasaire)   <img src="https://media.giphy.com/media/dxn6fRlTIShoeBr69N/giphy.gif" width="40">




<hr>



